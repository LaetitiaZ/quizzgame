import assert = require("assert");
import {Poll} from "../src/entities/Poll";
import {Question} from "../src/entities/Question";
import fastify from "fastify";
import {addPoll, clearPolls, getPolls} from "../src/services/pollService";
import {randomInt} from "crypto";
import {User} from "../src/entities/User";
import {Repository} from "typeorm";
import {Answer} from "../src/entities/Answer";
import {addUser, clearUser} from "../src/services/userService";
import * as LightMyRequest from 'light-my-request'

const server = fastify()
server.register(require("../src/plugins/db"))
server.register(require('../src/plugins/auth'))
server.register(require('../src/plugins/routes'))

declare module 'fastify' {
    export interface FastifyInstance {
        verifyJWT(): void,

        setAuthCookie(rpl: any, user: User): void,

        verifyUserOwnPoll(): void,

        db: {
            users: Repository<User>
            polls: Repository<Poll>
            answer: Repository<Answer>
            question: Repository<Question>
        }

    }

    interface FastifyRequest {
        user: User
    }

}

const createUser = (login: string, password: string): Promise<User> => {
    return addUser(server, "joe", login, password)
}

const login = (login: string, password: string): Promise<LightMyRequest.Response> => {
    return server.inject({
        method: 'POST',
        url: '/login',
        payload: {
            email: login,
            password: password
        }
    });
}

const setCookiesFromLogin = (response: LightMyRequest.Response): { [p: string]: string } => {
    let cookies: { [p: string]: string } = {}
    const authCookie = response.cookies.pop()
    if (authCookie) {
        let rawCookie = JSON.stringify(authCookie)
        let jsonObject = JSON.parse(rawCookie)
        cookies = {
            'auth-cookie': jsonObject.value
        }
    }
    return cookies;
}

describe('Poll controller', function () {
        afterEach(async function () {
            await clearPolls(server)
            await clearUser(server)
        })
        describe('get /polls', function () {
            it('should return status code 200 for GET request /polls with no authentication JWT', async function () {
                const response = await server.inject({
                    method: 'GET',
                    url: '/polls'
                })
                assert.ok(response.statusCode == 200)
                assert.ok(response.body === '[]')
            })
        })
        describe('post /poll', function () {
            describe('unauthenticated', function () {
                it('should return status code 401 for unauthenticated POST request /polls', async function () {
                    const response = await server.inject({
                        method: 'POST',
                        url: '/poll',
                        payload: {
                            title: 'testingPoll',
                            questions: [new Question("testingQuestion")]
                        }

                    })
                    console.log(response.statusCode)
                    assert.ok(response.statusCode == 401)
                })
            })
            describe('authenticated', function () {
                let id: string
                let cookies: { [p: string]: string }
                beforeEach(async function () {
                    let mail = "mail"
                    let password = "mdp"

                    let user = await createUser(mail, password)
                    id = user.id
                    let response = await login(mail, password)

                    cookies = setCookiesFromLogin(response)
                    assert.ok(response.statusCode == 200)
                })

                it('should return status code 200 for POST request /polls with valid authentication', async function () {
                    const response = await server.inject({
                        method: 'POST',
                        url: '/poll',
                        payload: {
                            title: 'testingPoll',
                            questions: [new Question("testingQuestion")]
                        },
                        cookies: cookies
                    })

                    console.log(cookies)
                    console.log(response.statusCode)
                    assert.ok(response.statusCode == 200)
                })
                it('should retrieve poll after POST request /polls', async function () {
                    await server.inject({
                        method: 'POST',
                        url: '/poll',
                        payload: {
                            title: 'testingPoll',
                            questions: [new Question("testingQuestion")]
                        },
                        cookies: cookies

                    })

                    const response = await server.inject({
                        method: 'GET',
                        url: '/polls',
                    })
                    assert.ok(response.statusCode == 200)
                    assert.ok(response.body.includes("\"title\":\"testingPoll\""))
                })
            })
        })

        describe('/delete polls', function () {
            describe('unauthenticated', function () {
                it('should return status code 401 for DELETE existent poll', async function () {
                    let poll = await addPoll(server, "title", [new Question("testingQuestion")], await createUser("anonymous", "anomymous"))
                    const deleteResponse = await server.inject({
                        method: 'DELETE',
                        url: '/poll/' + poll.id
                    })
                    assert.ok(deleteResponse.statusCode == 401)
                })
            })
            describe('authenticated and allowed', function () {
                let user: User
                let cookies: { [p: string]: string }
                beforeEach(async function () {
                    let mail = "mail"
                    let password = "mdp"

                    user = await createUser(mail, password)
                    let response = await login(mail, password)

                    cookies = setCookiesFromLogin(response)
                    assert.ok(response.statusCode == 200)
                })

                it('should return status code 200 for DELETE existent poll if poll is from current users', async function () {
                    let poll = await addPoll(server, "title", [new Question("testingQuestion")], user)
                    const deleteResponse = await server.inject({
                        method: 'DELETE',
                        url: '/poll/' + poll.id,
                        cookies: cookies
                    })
                    let pollsAfterDelete = await getPolls(server)
                    assert.ok(deleteResponse.statusCode == 200)
                    assert.ok(pollsAfterDelete.length === 0)
                })

                it('should return status code 404 for DELETE unknown poll id', async function () {
                    const d = await server.inject({
                        method: 'DELETE',
                        url: '/poll/' + randomInt(10),
                        cookies: cookies
                    })
                    console.log(d.statusCode)
                    assert.ok(d.statusCode == 404)
                })
            })
        })
        describe('post /login', function () {
            it("should accept good credentials", async function () {
                let mail = "mail"
                let password = "password"
                await createUser(mail, password);

                let response = await login(mail, password);
                assert.ok(response.statusCode === 200)
            })
            it("should return 401 when wrong credentials", async function () {
                let mail = "mail"
                let password = "password"
                await createUser(mail, password);

                let wrongMail = "wrong mail"
                let wrongPassword = "wrong password"

                let response = await login(wrongMail, wrongPassword);
                console.log(response.statusCode)
                assert.ok(response.statusCode === 401)
            })
        })
    }
)
