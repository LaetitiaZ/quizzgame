import chai = require("chai");
import chaiHttp = require("chai-http");
import {dependencies, devDependencies} from "../package.json"
import {assert} from "chai";

chai.use(chaiHttp)

const apiURL: string = "https://api.npmjs.org/downloads/point/last-week/"

const checkWeeklyDownload = (dependencies: object) => {
    for (let dependency in dependencies) {
        chai.request(apiURL)
            .get(dependency)
            .end((err, res) => {
                let downloads = res.body.downloads
                console.log(dependency + " last week downloads: " + downloads)
                assert.ok(downloads > 50000)
            })
    }
}

describe('Dependencies', function () {
    describe('check weekly downloads', function () {
        it('should be over 50k for each installed dependencies', function (done) {
            checkWeeklyDownload(dependencies)
            done()
        })
        it('should be over 50k for each installed devDependencies', function (done) {
            checkWeeklyDownload(devDependencies)
            done()
        })
    })
})