import * as dotenv from 'dotenv'
import {config} from 'dotenv'
import * as path from 'path'

export const NODE_ENV = (process.env.NODE_ENV || 'development') as 'test' | 'development' | 'production'

// config() does not override loaded env variable, so load overrides first
if (NODE_ENV === 'test' || NODE_ENV === 'development') config({ path: path.resolve(process.cwd(), '.env.test') })
config()

export const DATABASE_TYPE = getOrThrow('DATABASE_TYPE')
export const DATABASE_HOST = getOrThrow('DATABASE_HOST')
export const DATABASE_PORT = parseInt(getOrThrow('DATABASE_PORT'), 10)
export const DATABASE_USER = getOrThrow('DATABASE_USER')
export const DATABASE_PASS = getOrThrow('DATABASE_PASS')
export const DATABASE_NAME = getOrThrow('DATABASE_NAME')
export const DATABASE_LOGGING = process.env.DATABASE_LOGGING === 'true'
export const DATABASE_SYNC = process.env.DATABASE_SYNC === 'true'

export const COOKIE_NAME = getOrThrow('COOKIE_NAME')
export const AUTH_COOKIE_NAME = getOrThrow('AUTH_COOKIE_NAME')


function getOrThrow(name: string) {
  const val = process.env[name]
  if (typeof val === 'undefined') throw new Error(`Missing mandatory environment variable :` + name)
  return val
}