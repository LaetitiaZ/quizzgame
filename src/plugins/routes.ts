import {Question} from "../entities/Question";

import fastifyPlugin from "fastify-plugin";

import * as pollService from "../services/pollService";
import {deletePoll} from "../services/pollService";
import * as userService from "../services/userService";
import {User} from "../entities/User";

//import {server} from '../index';
import {FastifyInstance} from 'fastify';
import * as SignJSONSchemaValidation from '../schemas/sign.json'
import * as LoginJSONSchemaValidation from '../schemas/login.json'
import * as CreatePollSchemaValidation from '../schemas/createPoll.json'
import * as DeletePollSchemaValidation from '../schemas/deletePoll.json'

import {SignJSONSchema as SignSchemaInterface} from '../types/sign'
import {LoginJSONSchema as LoginSchemaInterface} from '../types/login'
import {DeletePollJSONSchema as DeleteSchemaInterface} from '../types/deletePoll'
import {CreatePollJSONSchema as CreatePollSchemaInterface} from '../types/createPoll'


/*eslint-disabled*/

module.exports = fastifyPlugin((server: FastifyInstance, opts: any, next: any) => {

    server.post<{ Body: SignSchemaInterface }>('/sign',
        {schema: {body: SignJSONSchemaValidation}},
        async (request, reply) => {
            process.stdout.write("received request")

            const user: User = await userService.addUser(server, request.body.name, request.body.email, request.body.password)
            if (user)
                return reply.send({"message": "Successful sign-in !"})
            else {
                reply.statusCode = 400;
                return reply.send({"message": "An error as occured"})
            }

        })

    server.post<{ Body: LoginSchemaInterface }>('/login',
        {schema: {body: LoginJSONSchemaValidation}},
        async (request, reply) => {

            await userService.checkUser(server, request.body.email, request.body.password)
                .then(user => {
                    server.setAuthCookie(reply, user);
                    return reply.send({"message": "account logged-in", "email": user.email});
                }).catch(() => {
                    reply.statusCode = 401;
                    return reply.send({'message': "wrong credentials"})
                })
        })

    server.get('/polls',
        async (request, reply) => {
            process.stdout.write("received request")
            const polls = await pollService.getPolls(server)
            await reply.send(polls)
        })

    server.post<{ Body: CreatePollSchemaInterface }>('/poll',
        {
            // eslint-disable-next-line @typescript-eslint/unbound-method
            preValidation: [server.verifyJWT],
            schema: {body: CreatePollSchemaValidation}
        },
        async (request, reply) => {
            process.stdout.write("received request")

            // Create Poll
            const questions = request.body.questions.map(question => new Question(question.content))
            const poll = await pollService.addPoll(server, request.body.title, questions, request.user)
            await reply.send(poll)
        })


    server.delete<{ Params: DeleteSchemaInterface }>('/poll/:id',
        {
            preValidation: [server.verifyJWT, server.verifyUserOwnPoll],
            schema: {params: DeletePollSchemaValidation}
        },
        async (request, reply) => {
            const poll_id = request.params.id;
            await deletePoll(server, poll_id)
                .then(() => {
                    reply.statusCode = 200
                    return reply.send()
                })
                .catch(() => {
                    reply.statusCode = 404
                    return reply.send()
                })
        })

    next()
})