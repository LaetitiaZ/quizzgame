import {AUTH_COOKIE_NAME, COOKIE_NAME} from "../dotenv";
import {User} from "../entities/User";
import * as tokenManager from './tokenManager';
import {FastifyInstance, FastifyReply} from 'fastify';
import fp from "fastify-plugin";
import fastifyCookie = require("fastify-cookie")
/*eslint-disable*/


module.exports = fp(function (fastify: FastifyInstance, opts: any, next: any) {
    // your plugin code
    fastify.register(require('fastify-cookie'))
    fastify.decorate("setAuthCookie", function (rpl: FastifyReply, user: User) {
        const token = tokenManager.createToken(user); //create a token with custom data
        rpl.setCookie(COOKIE_NAME, token, {
            httpOnly: true,
        });
    });


    fastify.decorate("verifyJWT", (req: any, res: any, done: any) => {
        const cookie = req.cookies[AUTH_COOKIE_NAME];
        console.log(req.cookies)
        const callback = async ({userId, err}: any) => {
            if (userId){
                req.user = await fastify.db.users.findOneOrFail(userId); 
                done();
            } 
            if (err) 
            {
                res.statusCode = 401
                done({'message': 'not authentified'})
            }
        }
        tokenManager.verifyToken(cookie, callback)
    });
    
    fastify.decorate("verifyUserOwnPoll", async (req: any, res: any, done: any) => {
        const user: User = req.user
        const poll_id = req.params.id
        if(poll_id){
            const poll = await fastify.db.polls.findOne(poll_id, {relations: ['user']});
            if (poll){
                if(poll.user.id === user.id)
                    done();
                else{
                    res.statusCode = 401                    
                    done({'message' :"user do not own this poll"})
                }
            }
            else{
                res.statusCode = 404
                done({'message':"poll do not exist"});
            }
        }
    });


    next()
})
