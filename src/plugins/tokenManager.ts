import fastify from "fastify";
import * as jwt from 'jsonwebtoken';
import {User} from "../entities/User";
import * as fs from 'fs';
import * as path from 'path';

const privateKey = fs.readFileSync(
    path.join(__dirname, "../../keys/rsa.private"),
    "utf8"
);

const publicKey = fs.readFileSync(
    path.join(__dirname, "../../keys/rsa.public"),
    "utf8"
);


export const createToken = (user: User) : string => {
    const userId: string = user.id;
    const token: string = jwt.sign(
        {
            userId: userId
        },
        privateKey,
        { algorithm: "RS256", expiresIn: "2h" }
    );
    return token;
};


export const verifyToken = (token: string, callback: ({ err, userId }: any) => void) => {
    jwt.verify(
        token,
        publicKey,
        { algorithms: ["RS256"] },
        (err: any, decoded: any) => {
            if (err !== null) {
                return callback({ err: "unauthorized" });
            }
            return callback({ userId: decoded["userId"] || "" });
        }
    );
}; 