import "reflect-metadata"
import fp from "fastify-plugin"
import {createConnection, DatabaseType, Repository} from "typeorm"
import {Poll} from "../entities/Poll"
import {User} from "../entities/User"
import {Answer} from "../entities/Answer"
import {Question} from "../entities/Question"
import * as dotenv from '../dotenv'


module.exports = fp(async server => {
    try {
        let dataBaseType: DatabaseType
        switch (dotenv.DATABASE_TYPE)  {
            case "sqlite" : dataBaseType = "sqlite"
                break
            default: dataBaseType = "mysql"
        }

        const connectionOptions = {
            type: dataBaseType,
            host: dotenv.DATABASE_HOST,
            port: dotenv.DATABASE_PORT,
            username: dotenv.DATABASE_USER,
            password: dotenv.DATABASE_PASS,
            database: dotenv.DATABASE_NAME,
            entities: [User, Poll, Answer, Question],
            synchronize: dotenv.DATABASE_SYNC,
            logging: dotenv.DATABASE_LOGGING,
            multipleStatements: true
        }
        const connection = await createConnection(connectionOptions)

        // this object will be accessible from any fastify server instance
        server.decorate("db", {
            users: connection.getRepository(User),
            polls: connection.getRepository(Poll),
            question: connection.getRepository(Question),
            answer: connection.getRepository(Answer)
        })
    } catch (error) {
      console.log(error)
    }
  })