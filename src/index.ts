import fastify from "fastify";
// import * as fastifyCookie from "fastify-cookie";
import {User} from "./entities/User";
import {Repository} from "typeorm";
import {Poll} from "./entities/Poll";
import {Answer} from "./entities/Answer";
import {Question} from "./entities/Question";
import * as fastifyCookie from 'fastify-cookie'
import * as fastifySwagger from 'fastify-swagger'
import * as swaggerOption from './swagger_options';

/*eslint-disable*/
export const server = fastify()

declare module 'fastify' {
    export interface FastifyInstance {
        verifyJWT(): void,
        setAuthCookie(rpl: any, user: User): void,
        verifyUserOwnPoll(): void,
        db: {
            users: Repository<User>
            polls: Repository<Poll>
            answer: Repository<Answer>,
            question: Repository<Question>
        }
    }

    interface FastifyRequest {
        user: User
    }
}

// ------------------ PLUGINS ----------------//
// ADD DB
server.register(require("./plugins/db"))

// ADD DECORATOR
server.register(
    require('./plugins/auth')
)

// SWAGGER DOCUMENTATION
server.register(require('fastify-swagger'), swaggerOption.options);

// ADD ROUTES
server.register(
    require('./plugins/routes')
)

// ADD COOKIE LIB
server.register(require('fastify-cookie'))

// ------------------ LAUNCH LISTENER ----------------//
server.listen(3000, (err) => {
    if (err) {
        server.log.error(err)
        process.exit(1)
    }
})

module.exports = server

