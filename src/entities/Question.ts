import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Poll} from "./Poll";
import {Answer} from "./Answer";


@Entity()
export class Question {
    // ---------------- COLUMNS ---------------- //
    constructor(content: string) {
        this.content = content;
    }

    @PrimaryGeneratedColumn()
    id!: string

    @Column()
    content: string

// ---------------- RELATIONS ---------------- //

    @OneToMany(() => Answer, answer => answer.question, {
        cascade: true
    })
    answers: Answer[]

    @ManyToOne(() => Poll, poll => poll.questions, { onDelete: 'CASCADE', onUpdate:'CASCADE' })
    poll: Poll


}