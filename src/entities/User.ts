import {Column, Entity, JoinTable, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Poll} from './Poll';
import {Answer} from './Answer';

@Entity()
export class User {

// ---------------- COLUMNS ---------------- //

    @PrimaryGeneratedColumn()
    id!: string

    @Column()
    name: string

    @Column()
    email: string

    @Column()
    password: string

    // ---------------- RELATIONS ---------------- //

    @OneToMany(() => Poll, poll => poll.user, {onDelete:"CASCADE", onUpdate:"CASCADE"})
    polls: Poll[];


    @OneToMany(() => Answer, answer => answer.user, {onDelete:"CASCADE", onUpdate:"CASCADE"})
    answers: Answer[];

    constructor(name: string, email: string, password: string) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

}