import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm"
import {Question} from './Question';
import {User} from './User';

@Entity()
export class Answer {
    // ---------------- COLUMNS ---------------- //

    @PrimaryGeneratedColumn()
    id!: string

    @Column()
    content: string

    // ---------------- RELATIONS ---------------- //

    @ManyToOne(() => Question, question => question.answers)
    question: Question

    @ManyToOne(() => User, user => user.answers)
    user: User

}

