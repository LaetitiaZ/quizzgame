import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, JoinTable} from "typeorm";
import {Question} from './Question';
import {User} from './User';


@Entity()
export class Poll {
    // ---------------- COLUMNS ---------------- //

    @PrimaryGeneratedColumn()
    id!: string

    @Column()
    title: string

    @CreateDateColumn()
    created_at: Date

    // ---------------- RELATIONS ---------------- //

    @OneToMany(() => Question, question => question.poll)
    @JoinTable()
    questions: Array<Question>

    @ManyToOne(() => User, user => user.polls)
    @JoinTable()
    user: User;


    constructor(title: string, questions: Array<Question>, user: User) {
        this.title = title;
        this.questions = questions;
        this.user = user;
    }
}
