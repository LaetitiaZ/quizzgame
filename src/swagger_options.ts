export const options = {
        routePrefix: '/documentation',
        exposeRoute: true,
        swagger:{
            info: {
                title: "Quizzgame",
                description : "Quizzgame API documentation",
                version: "1.0.0",
            }
        },
        externalDocs:{
            url: "https://swagger.io",
            description: "Find more info about Swagger here"
        },
        host: 'localhost:3000',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json']
}


