import {FastifyInstance} from "fastify";
import {User} from "../entities/User";
import * as CryptoJS from 'crypto-js';
import {DeleteResult} from "typeorm";


export const addUser = (server: FastifyInstance, name: string, email: string, password: string): Promise<User> => {
    const hashedPassword: string = CryptoJS.SHA256(password).toString();
    const user: User = new User(name, email, hashedPassword);
    return server.db.users.save(user)
}

export const checkUser = (server: FastifyInstance, email: string, password: string): Promise<User> => {
    const hashedPassword: string = CryptoJS.SHA256(password).toString();
    return server.db.users.findOneOrFail({where: [{email: email, password: hashedPassword}]})
}

export const deleteUser = (server: FastifyInstance, userId: string): Promise<DeleteResult> => {
     return server.db.users.delete(userId);
}

export const clearUser = (server:FastifyInstance) => {
    return server.db.users.clear()
}
