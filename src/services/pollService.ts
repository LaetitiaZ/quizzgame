import {Poll} from "../entities/Poll";
import {User} from "../entities/User";
import {FastifyInstance} from "fastify";
import {Question} from "../entities/Question";
import {DeleteResult} from "typeorm";


export const getPollsWithQuestions = (server: FastifyInstance): Promise<Poll[]> => {
    return server.db.polls.find({relations: ["questions"]})
}

export const getPolls = (server: FastifyInstance): Promise<Poll[]> => {
    return server.db.polls.find()
}

export const getPollById = (server: FastifyInstance, id: string): Promise<Poll | undefined> => {
    return server.db.polls.findOneOrFail(id)
}

export const addPoll = (server: FastifyInstance, title: string, questions: Question[], user: User): Promise<Poll> => {
    const poll_created = new Poll(title, questions, user);
    return server.db.polls.save(poll_created)
}

export const clearPolls = (server: FastifyInstance): Promise<void> => {
    return server.db.polls.clear()
}

export const deletePoll = (server: FastifyInstance, id: string): Promise<DeleteResult> => {
    return getPollById(server, id)
        .then(() => server.db.polls.delete(id));
}